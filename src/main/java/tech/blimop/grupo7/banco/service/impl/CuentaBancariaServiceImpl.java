package tech.blimop.grupo7.banco.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Optional;
import tech.blimop.grupo7.banco.entity.Cliente;
import tech.blimop.grupo7.banco.entity.CuentaBancaria;
import tech.blimop.grupo7.banco.repository.ClienteRepository;
import tech.blimop.grupo7.banco.repository.CuentaBancariaRepository;
import tech.blimop.grupo7.banco.service.CuentaBancariaService;

@Service
public class CuentaBancariaServiceImpl implements CuentaBancariaService{

	@Autowired
    private CuentaBancariaRepository cuentaR;
	@Autowired
    private ClienteRepository clienteRepository;
	@Autowired
	public CuentaBancaria addCuentaBancaria(CuentaBancaria cuenta) {
		
		Optional<Cliente> cliente = clienteRepository.findById(cuenta.getCliente().getId());

		CuentaBancaria newCuenta = new CuentaBancaria();
		newCuenta.setId(cuenta.getId());
		newCuenta.setNumeroCuenta(cuenta.getNumeroCuenta());
		newCuenta.setSaldo(cuenta.getSaldo());
		newCuenta.setCliente(cliente.get());
		newCuenta = cuentaR.save(newCuenta);

		return newCuenta;
	}

	public void save(Cliente cliente) {
		// TODO Auto-generated method stub
		
	}

}
