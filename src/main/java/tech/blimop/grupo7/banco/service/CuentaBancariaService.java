package tech.blimop.grupo7.banco.service;

import tech.blimop.grupo7.banco.entity.Cliente;
import tech.blimop.grupo7.banco.entity.CuentaBancaria;

public interface CuentaBancariaService {
	
	CuentaBancaria addCuentaBancaria(CuentaBancaria cuenta);
	
    void save(Cliente cliente);
}
